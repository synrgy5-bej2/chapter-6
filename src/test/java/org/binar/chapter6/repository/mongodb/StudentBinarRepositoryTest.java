package org.binar.chapter6.repository.mongodb;

import org.binar.chapter6.model.StudentBinar;
import org.binar.chapter6.repository.mongorepo.StudentBinarRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class StudentBinarRepositoryTest {

    @Autowired
    StudentBinarRepository studentBinarRepository;

    @Test
    void testInsert() {
//        StudentBinar studentBinar = new StudentBinar("rizkyfauzi", "Back End Java 2",
//                5, "Synrgy", 25);
        StudentBinar studentBinar = new StudentBinar();
        studentBinar.setUsername("rizkyfauzi");
        studentBinar.setKelas("Back End Java 2");
        studentBinar.setBatch(5);
        studentBinar.setProgram("Synrgy");
        studentBinar.setUsia(25);

        studentBinarRepository.save(studentBinar);
    }
}
