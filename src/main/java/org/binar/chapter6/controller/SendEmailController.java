package org.binar.chapter6.controller;

import org.binar.chapter6.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/email")
public class SendEmailController {

    @Autowired
    EmailService emailService;

    @PostMapping("/send-email")
    public String sendEmail(@RequestBody Map<String, Object> body) {
        String from = body.get("from").toString();
        String pass = body.get("password").toString();
        String to = body.get("to").toString();
        String subject = body.get("subject").toString();
        String message = body.get("msg").toString();
        emailService.send(from, pass, to, subject, message);
        return "ok";
    }
}
