package org.binar.chapter6.repository.mongorepo;

import org.binar.chapter6.model.StudentBinar;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentBinarRepository extends MongoRepository<StudentBinar, String> {
}
